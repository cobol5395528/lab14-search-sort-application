       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GENREP.
       AUTHOR. PHADOL WONGSIRI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "product.csv"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-100-INPUT-STATUS.
           SELECT 200-INPUT-FILE ASSIGN TO "transac.csv"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-200-INPUT-STATUS.
           SELECT 300-OUTPUT-FILE ASSIGN TO "report.rpt"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-300-OUTPUT-STATUS.
           
       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD.
           05 100-PRODUCT-ID       PIC X(4)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 100-PRODUCT-NAME     PIC X(15)   VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 100-PRODUCT-PRICE    PIC 9V99    VALUE ZERO.

       FD  200-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  200-INPUT-RECORD.
           05 200-TRN-ID           PIC X(5)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 200-YMD-HHMN         PIC X(14)   VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 200-PRODUCT-ID       PIC X(4)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 200-QTY              PIC X(2)    VALUE SPACE.

       FD  300-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  300-OUTPUT-RECORD       PIC X(33)   VALUE SPACES.

       WORKING-STORAGE SECTION. 
       01  WS-CALCULATION.
           05 WS-PRODUCT-MAX       PIC 9(3)    VALUE 5.
           05 WS-TRN-MAX           PIC 9(3)    VALUE 50.
           05 WS-PRODUCT-CB        PIC X(4)    VALUE SPACE.
           05 WS-QTY-TOTAL         PIC 9(3)    VALUE ZERO.
           05 WS-LINE-TOTAL        PIC 9(4)V99 VALUE ZERO.
           05 WS-TOTAL             PIC 9(5)V99 VALUE ZERO.
       
       01  WS-MATCH-PRODUCT.
           05 WSM-PRODUCT-ID       PIC X(4)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSM-PRODUCT-NAME     PIC X(15)   VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSM-PRODUCT-PRICE    PIC 9V99    VALUE ZERO.

       01  WS-100-INPUT-STATUS     PIC X(2).
           88 FILE-OK                          VALUE "00".
           88 FILE-AT-END                      VALUE "10".

       01  WS-200-INPUT-STATUS     PIC X(2).
           88 FILE-OK                          VALUE "00".
           88 FILE-AT-END                      VALUE "10".

       01  WS-300-OUTPUT-STATUS    PIC X(2).
           88 FILE-OK                          VALUE "00".
           88 FILE-AT-END                      VALUE "10".

       01  WS-ARRAY-PRODUCT OCCURS 5 TIMES INDEXED BY WS-ID-PROD.
           05 WSA-PRODUCT-ID       PIC X(4)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSA-PRODUCT-NAME     PIC X(15)   VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSA-PRODUCT-PRICE    PIC 9V99    VALUE ZERO.

       01  WS-ARRAY-TRN OCCURS 50 TIMES INDEXED BY WS-ID-TRN.
           05 WSA-TRN-ID           PIC X(5)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSA-YMD-HHMN         PIC X(14)   VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSA-PRODUCT-ID       PIC X(4)    VALUE SPACE.
           05 FILLER               PIC X       VALUE SPACE.
           05 WSA-QTY              PIC 9(2)    VALUE ZERO.

       01  RPT-FORMAT.
           05 RPT-PRODUCT-HEADER.
              10 RPT-PRODUCT-ID    PIC X(4)    VALUE SPACE.
              10 FILLER            PIC X(4)    VALUE SPACE.
              10 RPT-PRODUCT-NAME  PIC X(15)   VALUE SPACE.
              10 FILLER            PIC X(5)    VALUE SPACE.
              10 RPT-PRODUCT-PRICE PIC $9.99   VALUE ZERO.
           05 RPT-COL-HEADER       PIC X(33)
                    VALUE "#ID    D/M/Y HH:MN    QTY  TOTAL".
           05 RPT-DOT-LINE.
              10 FILLER            PIC X OCCURS 33 TIMES VALUE ".".
           05 RPT-DASH-LINE.
              10 FILLER            PIC X OCCURS 33 TIMES VALUE "-".
           05 RPT-PRODUCT-FOOTER.
              10 FILLER            PIC X(13)
                    VALUE "        QTY =".
              10 RPT-QTY-TOTAL     PIC ZZZ9    VALUE ZERO.
              10 FILLER            PIC X(7)
                    VALUE " TOTAL=".
              10 RPT-TOTAL         PIC $$$$$9.99      VALUE ZERO.
           05 RPT-TRN-LINE.
              10 RPT-TRN-ID        PIC X(5)    VALUE SPACE.
              10 FILLER            PIC X(2)    VALUE SPACE.
              10 RPT-YMD-HHMN      PIC X(14)   VALUE SPACE.
              10 FILLER            PIC X       VALUE SPACE.
              10 RPT-LINE-QTY      PIC ZZ9     VALUE ZERO.
              10 FILLER            PIC X       VALUE SPACE.
              10 RPT-LINE-TOTAL    PIC $$$9.99 VALUE ZERO.

       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
           PERFORM 3000-END     THRU 3000-EXIT
           GOBACK
           .

       1000-INITIAL.
           OPEN INPUT  100-INPUT-FILE
           OPEN INPUT  200-INPUT-FILE
           OPEN OUTPUT 300-OUTPUT-FILE
           
           PERFORM 8100-READ-PRODUCT THRU 8100-EXIT
           PERFORM 4100-LOAD-PRODUCT THRU 4100-EXIT  

           PERFORM 8200-READ-TRN THRU 8200-EXIT
           PERFORM 4200-LOAD-TRN THRU 4200-EXIT  
           .

       1000-EXIT.
           EXIT.

       2000-PROCESS.
      *    PERFORM 2910-DEBUG-PRODUCT-ARRAY THRU 2910-EXIT 
      *    PERFORM 2920-DEBUG-TRN-ARRAY THRU 2920-EXIT
           PERFORM VARYING WS-ID-TRN FROM 1 BY 1
              UNTIL WS-ID-TRN > WS-TRN-MAX
                 IF WS-PRODUCT-CB NOT = WSA-PRODUCT-ID
                         OF WS-ARRAY-TRN(WS-ID-TRN)
                    IF WS-PRODUCT-CB NOT = SPACE
      *    PERFORM END
                       PERFORM 2300-PROCESS-END THRU 2300-EXIT
                    END-IF
      *    PERFORM HEAD
                    PERFORM 2100-PROCESS-HEAD THRU 2100-EXIT
                 END-IF
      *    PERFORM DETAIL
                 PERFORM 2200-PROCESS-DETAIL THRU 2200-EXIT
           END-PERFORM
      *    PERFORM END
           PERFORM 2300-PROCESS-END THRU 2300-EXIT
           .

       2000-EXIT.
           EXIT.

       2100-PROCESS-HEAD.
           MOVE WSA-PRODUCT-ID OF WS-ARRAY-TRN(WS-ID-TRN)
              TO WS-PRODUCT-CB
           SET WS-ID-PROD TO 1
           SEARCH WS-ARRAY-PRODUCT
              WHEN WSA-PRODUCT-ID OF WS-ARRAY-PRODUCT(WS-ID-PROD)
                 = WS-PRODUCT-CB
               MOVE WS-ARRAY-PRODUCT(WS-ID-PROD) TO WS-MATCH-PRODUCT
           END-SEARCH

      *    DISPLAY WS-MATCH-PRODUCT 
           MOVE WSM-PRODUCT-ID TO RPT-PRODUCT-ID
           MOVE WSM-PRODUCT-NAME TO RPT-PRODUCT-NAME
           MOVE WSM-PRODUCT-PRICE TO RPT-PRODUCT-PRICE

           DISPLAY RPT-PRODUCT-HEADER 
           DISPLAY RPT-COL-HEADER 
           DISPLAY RPT-DOT-LINE

           MOVE RPT-PRODUCT-HEADER TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT
           MOVE RPT-COL-HEADER TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT
           MOVE RPT-DOT-LINE TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT

           MOVE ZERO TO WS-QTY-TOTAL
           .
       2100-EXIT.
           EXIT.

       2200-PROCESS-DETAIL.
      *    DISPLAY WSA-TRN-ID(WS-ID-TRN)                      " "
      *            WSA-YMD-HHMN(WS-ID-TRN)                    " "
      *            WSA-PRODUCT-ID OF WS-ARRAY-TRN(WS-ID-TRN)  " "
      *            WSA-QTY(WS-ID-TRN)                         " "
           
           COMPUTE WS-LINE-TOTAL = WSA-QTY(WS-ID-TRN)
                                      * WSM-PRODUCT-PRICE
           MOVE WSA-TRN-ID(WS-ID-TRN)       TO RPT-TRN-ID
           MOVE WSA-YMD-HHMN(WS-ID-TRN)     TO RPT-YMD-HHMN
           MOVE WSA-QTY(WS-ID-TRN)          TO RPT-LINE-QTY 
           MOVE WS-LINE-TOTAL               TO RPT-LINE-TOTAL

           DISPLAY RPT-TRN-LINE

           MOVE RPT-TRN-LINE TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT

           ADD WSA-QTY(WS-ID-TRN) TO WS-QTY-TOTAL
           .

       2200-EXIT.
           EXIT.

       2300-PROCESS-END.
      *    DISPLAY "END-PRODUCT TOTAL " WS-QTY-TOTAL
           COMPUTE WS-TOTAL = WS-QTY-TOTAL * WSM-PRODUCT-PRICE
           MOVE WS-QTY-TOTAL TO RPT-QTY-TOTAL
           MOVE WS-TOTAL     TO RPT-TOTAL

           DISPLAY RPT-PRODUCT-FOOTER 
           DISPLAY RPT-DASH-LINE

           MOVE RPT-PRODUCT-FOOTER TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT
           MOVE RPT-DASH-LINE TO 300-OUTPUT-RECORD 
           PERFORM 7300-WRITE THRU 7300-EXIT
           .

       2300-EXIT.
           EXIT.

           EXIT.


       
       3000-END.
           CLOSE 100-INPUT-FILE
           CLOSE 200-INPUT-FILE 
           CLOSE 300-OUTPUT-FILE
           .

       3000-EXIT.
           EXIT.

       4100-LOAD-PRODUCT.
           PERFORM VARYING WS-ID-PROD FROM 1 BY 1
              UNTIL WS-ID-PROD > WS-PRODUCT-MAX
              OR FILE-AT-END OF WS-100-INPUT-STATUS

                 MOVE 100-INPUT-RECORD TO WS-ARRAY-PRODUCT(WS-ID-PROD)
                 PERFORM 8100-READ-PRODUCT THRU 8100-EXIT
           END-PERFORM
           .

       4100-EXIT.
           EXIT.

       4200-LOAD-TRN.
           PERFORM VARYING WS-ID-TRN FROM 1 BY 1
              UNTIL WS-ID-TRN > WS-TRN-MAX
              OR FILE-AT-END OF WS-200-INPUT-STATUS

                 MOVE 200-INPUT-RECORD TO WS-ARRAY-TRN(WS-ID-TRN)
                 PERFORM 8200-READ-TRN THRU 8200-EXIT
           END-PERFORM
           SORT WS-ARRAY-TRN ON ASCENDING KEY
                          WSA-PRODUCT-ID OF WS-ARRAY-TRN 
                          WSA-TRN-ID
           .

       4200-EXIT.
           EXIT.

       7300-WRITE.
           WRITE 300-OUTPUT-RECORD
           .

       7300-EXIT.
           EXIT.

       8100-READ-PRODUCT.
           READ 100-INPUT-FILE
           .

       8100-EXIT.
           EXIT.

       8200-READ-TRN.
           READ 200-INPUT-FILE
           .

       8200-EXIT.
           EXIT.